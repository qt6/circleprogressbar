#include "circleprogressbar.h"

#include <QPaintEvent>
#include <QDebug>
#include <QPainter>

CircleProgressBar::CircleProgressBar(QWidget *parent)
    : QWidget(parent)
{
    setWidth();
    setPrimaryColor(Qt::gray);
    setSecondaryColor(Qt::yellow);
    setMinimumSize(300,300);


    angle=0;

}

void CircleProgressBar::setWidth(int width)
{
    m_wid=width;
}

void CircleProgressBar::setValue(float value)
{
    if(value && value<=1){
        angle=value*360;
        repaint();
    }
}

void CircleProgressBar::setPrimaryColor(QBrush brush)
{
    m_pBrush=brush;
}

void CircleProgressBar::setSecondaryColor(QBrush brush)
{
    m_sBrush=brush;
}

void CircleProgressBar::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
//    QPoint center=rect().center();

    painter.setRenderHints(QPainter::Antialiasing);


    painter.setBrush(m_sBrush);
    painter.drawEllipse(rect().center(),rect().width()/2,rect().height()/2);

    painter.setBrush(m_pBrush);
    painter.drawPie(rect(),90*16,-angle*16);
    painter.setBrush(Qt::white);
    painter.drawEllipse(rect().center(),rect().width()/2-m_wid*2,rect().height()/2-m_wid*2);


    painter.setBrush(Qt::NoBrush);
    painter.drawText(rect(),Qt::AlignCenter,QString("%0 %").arg(angle/360*100));



}
