#ifndef ROUNDPROGRESSBAR_H
#define ROUNDPROGRESSBAR_H

#include <QProgressBar>

class CircleProgressBar : public QWidget
{
    Q_OBJECT
public:
    explicit CircleProgressBar(QWidget *parent = nullptr);

    void setWidth(int width =20);
    void setValue(float value);
    void setPrimaryColor(QBrush brush);
    void setSecondaryColor(QBrush brush);

signals:

private:

    void paintEvent(QPaintEvent *event) override;

    int m_wid;
    float angle;
    QBrush m_pBrush;
    QBrush m_sBrush;

};

#endif // ROUNDPROGRESSBAR_H
