#include "widget.h"

#include <QVBoxLayout>
#include "circleprogressbar.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{

auto mainLay=new QVBoxLayout(this);

auto rPB=new CircleProgressBar(this);

mainLay->addWidget(rPB);

rPB->setValue(0.1);


}

Widget::~Widget()
{
}

